FROM composer:latest AS composer

FROM php:7.4-fpm

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN apt-get update && apt-get install -qy \
    git zip unzip curl libfreetype6-dev libmcrypt-dev libxml2-dev wget libc-client-dev libkrb5-dev zlib1g-dev libzip-dev \
    libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb libonig-dev \
    && rm -r /var/lib/apt/lists/*

RUN yes | pecl install mcrypt

RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install mbstring tokenizer pdo_mysql exif bcmath sockets imap zip

ENV COMPOSER_ALLOW_SUPERUSER=1
